import { PCP } from "../src/data";
import { check_password } from "../src/validate";

describe("Validate test", () => {
  test("testing simple PCP", () => {
    const pcp = PCP.parse("{\"min_length\": 6}");
    expect(check_password("abcdef", pcp)).toBe(true);
    expect(check_password("abcde", pcp)).toBe(false);
  });
});

describe("Validate test", () => {
  test("testing standard PCP 1", () => {
    const pcp = PCP.parse("{\"min_length\": 8, \"max_length\": 20, \"require_subset\": {\"count\": 2}}");
    expect(check_password("abcde123", pcp)).toBe(true);
    expect(check_password("abcdefgh", pcp)).toBe(false);
    expect(check_password("abcde", pcp)).toBe(false);
    expect(check_password("abcdefghijklmnopqrstuvwxyz", pcp)).toBe(false);
  });
});

describe("Validate test", () => {
  test("testing standard PCP 2", () => {
    const pcp = PCP.parse("{\"min_length\": 8, \"max_consecutive\": 7}");
    expect(check_password("abcdefgh", pcp)).toBe(true);
    expect(check_password("aaaaaaaa", pcp)).toBe(false);
  });
});
