import { PCP } from "../src/data";

describe("PCP test", () => {
  test("testing simple PCP", () => {
    const pcp = PCP.parse("{\"min_length\": 6}");
    expect(pcp.stringify()).toBe("{\"min_length\":6}");
  });
});

describe("PCP test", () => {
  test("testing standard PCP 1", () => {
    const pcp = PCP.parse("{\"min_length\": 8, \"max_length\": 20, \"require_subset\": {\"count\": 2}}");
    expect(pcp.stringify()).toBe("{\"min_length\":8,\"max_length\":20,\"require_subset\":{\"count\":2}}");
  });
});

describe("PCP test", () => {
  test("testing standard PCP 2", () => {
    const pcp = PCP.parse("{\"min_length\": 8, \"max_consecutive\": 7}");
    expect(pcp.stringify()).toBe("{\"min_length\":8,\"max_consecutive\":7}");
  });
});
